from L3.Utils.LinearizedDirectionMethod import linearized_direction_method
import matplotlib.pyplot as pp
from L3.Utils.helperFuncs import BasicPlot, dotest

points = dotest(linearized_direction_method)
BasicPlot()
pp.plot(points[0], points[1], 'ro-')
pp.show()
