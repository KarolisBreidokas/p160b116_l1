import numpy as np
import matplotlib.pyplot as pp
import L3.Utils.AssignedFuncts as AF
from L3.Utils.helperFuncs import BasicPlot
from Global.helperFuncs import make_quiver
from L3.Utils.LinearizedDirectionMethod import findRoot, LDM_Direction_vcd
from L3.Utils.PlotConsts import X0

BasicPlot()
func_gradient = AF.df(X0)
a1_gradient = AF.dg1(X0)
d, _ = LDM_Direction_vcd(func_gradient, a1_gradient)
d_normalized = d / (np.sqrt(np.sum(d ** 2)))
make_quiver(X0, d_normalized, 'green')
t = [[X0[i], X0[i] + 100 * d_normalized[i]] for i in range(2)]
s = findRoot(lambda x: AF.g2(X0 + x * d_normalized), 0)
X1 = X0 + s * d_normalized
pp.plot(t[0], t[1])

make_quiver(X0, AF.df(X0), "red")
make_quiver(X0, AF.dg1(X0), "blue")
make_quiver(X0, d, "green")
pp.plot(X1[0], X1[1], "ro")
pp.show()
