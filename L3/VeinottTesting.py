from L3.Utils.VeinottModification import VeinottModification
import matplotlib.pyplot as pp
from L3.Utils.helperFuncs import BasicPlot, dotest

points = dotest(VeinottModification)
BasicPlot()
pp.plot(points[0], points[1], 'ro-')
pp.show()
