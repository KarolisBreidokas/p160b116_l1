import matplotlib.pyplot as pp
import L3.Utils.PlotConsts as cns
import L3.Utils.AssignedFuncts as AF
import numpy as np

from Global.helperFuncs import make_quiver
from L3.Utils.PlotConsts import X0


def lp_vectormul(left, right, n):
    return sum([left[i] * right[i] for i in range(n)])


def BasicPlot():
    X0 = cns.X0
    xp = np.arange(cns.xl, cns.xh, cns.step)
    yp = np.arange(cns.yl, cns.yh, cns.step)
    xx, xy = np.meshgrid(xp, yp)
    X = np.array([xx, xy])
    func_points = AF.f(X)
    pp.xlim(cns.xl, cns.xh)
    pp.ylim(cns.yl, cns.yh)
    pp.figure(1)
    pp.contour(xp, yp, func_points, cns.ISOLines)
    for g in AF.constrains:
        g_points = g(X)
        pp.contour(xp, yp, g_points, [0, 0.1], colors=['red', 'blue'])


def PlotWithVcts():
    BasicPlot()
    X0 = cns.X0
    func_gradient = AF.df(cns.X0)
    g1_gradient = AF.dg1(cns.X0)
    pp.plot(X0[0], X0[1], 'go')
    make_quiver(X0, func_gradient, "red")
    make_quiver(X0, g1_gradient, "blue")


def dotest(optimizationAlgoritm):
    points = [[X0[0]], [X0[1]], ]
    X = X0
    for i in range(10):
        found, X = optimizationAlgoritm(AF.f, AF.df, AF.constrains, AF.constraint_grad, X)
        if found:
            break
        points[0].append(X[0])
        points[1].append(X[1])

    for p in zip(points[0],points[1]):
        print(p)
    return points
