import numpy as np
import pulp
from scipy import optimize as op

from L3.Utils.helperFuncs import lp_vectormul


def findRoot(f, x0):
    step = 1e-3
    while np.sign(f(x0 + step)) == 1:
        x0 = x0 + step
    ans = op.brentq(f, x0, x0 + step, xtol=1e-10)
    return ans


def LDM_Direction_vcd(f_grad, g_grad):
    problem = pulp.LpProblem("FDR", pulp.constants.LpMaximize)
    lp_d = pulp.LpVariable.dicts('d', [0, 1], -1.0, 1.0)
    theta = pulp.LpVariable('theta')
    problem += lp_vectormul(lp_d, f_grad, 2) <= -theta
    problem += lp_vectormul(lp_d, g_grad, 2) >= theta
    problem += theta
    problem.solve()
    d = np.array([lp_d[i].varValue for i in range(2)])
    return d, theta.varValue


def linearized_direction_method(funcion, function_grad, constraints, constraint_grads, X0):
    ans = list(map(lambda x: x[1],
                   filter(lambda x: abs(x[0](X0)) <= 1e-10,
                          zip(constraints,
                              range(len(constraints))
                              )
                          )
                   )
               )
    if len(ans) == 0:
        return True, X0
    i = ans[0]
    func_gradient = function_grad(X0)
    a1_gradient = constraint_grads[i](X0)
    d, theta = LDM_Direction_vcd(func_gradient, a1_gradient)
    if theta <= 0:
        return True, X0
    d_normalized = d / (np.sqrt(np.sum(d ** 2)))
    s = findRoot(lambda s: min([g(X0 + s * d_normalized) for g in constraints]), 0)
    return False, (X0 + s * d_normalized)
