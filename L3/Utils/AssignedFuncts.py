import numpy as np


def f(x: np.array):
    return (x[0] - 4) ** 2 + (x[1] - 4) ** 2


def df(x: np.array):
    return np.array([
        2 * (x[0] - 4),
        2 * (x[1] - 4),
    ])


def g1(x: np.array):
    return 2 * x[0] - (x[1]) ** 2 - 1.1


def dg1(x: np.array):
    return np.array([
        2,
        -2 * (x[1]),
    ])


def g2(x: np.array):
    return 9 - 0.8 * ((x[0]) ** 2) - 2 * x[1]


def dg2(x: np.array):
    return np.array([
        -0.8 * 2 * x[0],
        -2,
    ])


def g3(x: np.array):
    return x[1] - x[0] + 1.5


def dg3(x: np.array):
    return np.array([
        -1,
        1,
    ])


constrains = [g1, g2, g3]
constraint_grad = [dg1, dg2, dg3]
