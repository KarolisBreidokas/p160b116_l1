import numpy as np
import pulp
from L3.Utils.LinearizedDirectionMethod import findRoot
from L3.Utils.helperFuncs import lp_vectormul


def Veinott_dirrection_vct(f_grad, cnstr_grads,cnstrs,cnt, X0):
    func_gradient = f_grad(X0)
    problem = pulp.LpProblem("FDR", pulp.constants.LpMaximize)
    lp_d = pulp.LpVariable.dicts('d', [0, 1], -1.0, 1.0)
    theta = pulp.LpVariable('theta')
    problem += lp_vectormul(lp_d, func_gradient, 2) <= -theta
    for j in range(cnt):
        a1_gradient = cnstr_grads[j](X0)
        problem += lp_vectormul(lp_d, a1_gradient, 2) + cnstrs[j](X0) >= theta
    problem += theta
    problem.solve()
    d = np.array([lp_d[i].varValue for i in range(2)])
    return d, theta.varValue


def VeinottModification(funcion, function_grad, constraints, constraint_grads, X0):
    constraintCount = len(constraints)
    ans = list(map(lambda x: x[1],
                   filter(lambda x: abs(x[0](X0)) <= 1e-10,
                          zip(constraints,
                              range(constraintCount)
                              )
                          )
                   )
               )
    if len(ans) == 0:
        return None
    d, theta = Veinott_dirrection_vct(function_grad, constraint_grads,constraints,constraintCount, X0)
    if theta <= 0:
        return True, X0
    d_normalized = d / (np.sqrt(np.sum(d ** 2)))
    s = findRoot(lambda s: min([c(X0 + s * d_normalized) for c in constraints]), 0)
    return False, (X0 + s * d_normalized)
