def makePermutation(k, n):
    """
    generates permutation list of size n based on Antoine Comeau method for mapping permutations to numbers
    https://antoinecomeau.blogspot.com/2014/07/mapping-between-permutations-and.html
    :param k: Permutation number
    :param n: Permutation size
    :return: permutation based on key k
    """
    elems = list(range(n))
    for i in range(n):
        idx = k % (n - i)
        k = k // (n - i)
        for j in range(idx,n-i-1):
            tmp = elems[j]
            elems[j] = elems[j+1]
            elems[j+1] = tmp
    return elems

