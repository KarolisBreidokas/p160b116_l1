from typing import List
from L5.Utils.Point2d import Point2d, getLenByPermutation
import L5.Utils.PlotConstraints as PC
import matplotlib.pyplot as pp
from L5.Utils.Permutations import makePermutation
from Global.helperFuncs import MakeImage


def PlotPoints(fignum: int, points: List[Point2d]):
    """

    displays points in plot
    :param fignum: figure number
    :param points: list of points
    :return: figure object
    """
    figure = pp.figure(fignum)
    pp.xlim(PC.xrange)
    pp.ylim(PC.yrange)
    plotPoints = PointsToPlot(points)
    pp.plot(plotPoints[0], plotPoints[1], 'bo')
    pp.show()


def PlotMovement(fignum: int, k, points: List[Point2d]):
    figure = pp.figure(fignum)
    pp.xlim(PC.xrange)
    pp.ylim(PC.yrange)
    perm = makePermutation(k, len(points))
    newpoints = []
    for i in perm:
        newpoints.append(points[i])
    plotPoints = PointsToPlot(newpoints)
    plotPoints[0].append(newpoints[0].x)
    plotPoints[1].append(newpoints[0].y)
    pp.plot(plotPoints[0], plotPoints[1], 'bo-')
    pp.show()


def PlotMovementByPermutation(fignum: int, permutation, points: List[Point2d]):
    figure = pp.figure(fignum)
    pp.xlim(PC.xrange)
    pp.ylim(PC.yrange)
    len = getLenByPermutation(permutation, points)
    pp.title(f"{fignum},[{len}]")
    tmpPoints = points.copy()
    newpoints = []
    for i in permutation:
        newpoints.append(tmpPoints[i])
        del tmpPoints[i]
    plotPoints = PointsToPlot(newpoints)
    plotPoints[0].append(newpoints[0].x)
    plotPoints[1].append(newpoints[0].y)
    pp.plot(plotPoints[0], plotPoints[1], 'bo-')
    pp.show()


def PlotMovementByPermutationImage(fignum: int, permutation, points: List[Point2d]):
    figure = pp.figure(fignum)
    pp.xlim(PC.xrange)
    pp.ylim(PC.yrange)
    len = getLenByPermutation(permutation, points)
    pp.title(f"{fignum},[{len}]")
    tmpPoints = points.copy()
    newpoints = []
    for i in permutation:
        newpoints.append(tmpPoints[i])
        del tmpPoints[i]
    plotPoints = PointsToPlot(newpoints)
    plotPoints[0].append(newpoints[0].x)
    plotPoints[1].append(newpoints[0].y)
    pp.plot(plotPoints[0], plotPoints[1], 'bo-')
    img = MakeImage(figure)
    pp.close(figure)
    return img


def PointsToPlot(points):
    plotPoints = [[], [], ]
    for point in points:
        plotPoints[0].append(point.x)
        plotPoints[1].append(point.y)
    return plotPoints
