from math import factorial
import L5.Utils.Point2d as P2d


def BruteforceTSP(points):
    n = len(points)
    res, minLen = 0, P2d.getLen(0, points)

    for i in range(1, factorial(n)):
        length = P2d.getLen(i, points)
        if length < minLen:
            minLen = length
            res = i

    return res, minLen
