import numpy as np
import random as rng
from L5.Utils.Permutations import makePermutation


class Point2d:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def len_between(l: Point2d, r: Point2d):
    return np.sqrt((l.x - r.x) ** 2 + (l.y - r.y) ** 2)


def _uniformTuple(x: tuple):
    xl, xh = x
    return rng.uniform(xl, xh)


def generatePointList(n, xrange: tuple, yrange: tuple):
    result = []
    for i in range(n):
        x = _uniformTuple(xrange)
        y = _uniformTuple(yrange)
        result.append(Point2d(x, y))
    return result


def getLen(k, points):
    n = len(points)
    perm = makePermutation(k, n)
    length = 0
    for i in range(-1, n - 1):
        length += len_between(points[perm[i]], points[perm[i + 1]])
    return length


def getLenByPermutation(permutation, points: list):
    pointCoppy = points.copy()
    newPoints=[]
    n=len(points)
    for i in permutation:
        newPoints.append(pointCoppy[i])
        del pointCoppy[i]
    length=0
    for i in range(-1, n - 1):
        length += len_between(newPoints[i], newPoints[i + 1])
    return length