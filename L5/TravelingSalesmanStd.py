from L5.Utils.Permutations import makePermutation
from L5.Utils.BruteForceTSP import BruteforceTSP
from L5.Utils.Ploting import PlotMovement, PlotPoints
from L5.Utils.Point2d import generatePointList, getLen
from L5.Utils.PlotConstraints import yrange, xrange, point_count
import matplotlib.pyplot as pp
import scipy.optimize as op
import numpy as np
import random as rng

points = generatePointList(point_count, xrange, yrange)

def fun(x):
    xn = int(np.round(x[0] - 0.5))
    n = getLen(xn, points)
    return n


PlotPoints(0, points)
res = op.differential_evolution(fun,
                                bounds=[(0, point_count + 1)],
                                maxiter=100000,
                                polish=False,
                                strategy='rand2bin',
                                mutation=(.2,1),
                                recombination=.2,
                                disp=True,
                                popsize=10)
x, bruteRes = BruteforceTSP(points)
xn = int(np.round(res.x[0] - 0.5))
PlotMovement(1, xn, points)
PlotMovement(2, x, points)
