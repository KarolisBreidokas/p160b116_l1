from L5.PYGA.AssignedFuncs import function
from L5.PYGA.GeneticAlgorithm import GA_Printable
import imageio

allImages = []
X, fun, images = GA_Printable("Min: control", 0, function, (.001, .3), 6, 5, 20, 0.005, 0.03, True)
allImages.extend(images)
print(f"control: X={X} f(X)={fun}")
X, fun, images = GA_Printable("Min: population=20", 1, function, (.001, .3), 6, 20, 20, 0.005, 0.03, True)
allImages.extend(images)
print(f"pop increase: X={X} f(X)={fun}")
X, fun, images = GA_Printable("Min: gene count=20", 2, function, (.001, .3), 20, 5, 20, 0.005, 0.03, True)
allImages.extend(images)
print(f"gene increase: X={X} f(X)={fun}")
X, fun, images = GA_Printable("Min: mutation rate=0.1%", 3, function, (.001, .3), 6, 5, 20, 0.001, 0.03, True)
allImages.extend(images)
print(f"mutation decease: X={X} f(X)={fun}")
X, fun, images = GA_Printable("Min: mating rate=0.3%", 4, function, (.001, .3), 6, 5, 20, 0.005, 0.003, True)
allImages.extend(images)
print(f"mating decease: X={X} f(X)={fun}")
X, fun, images = GA_Printable("Min: increasing iteration count", 4, function, (.001, .3), 20, 10, 30, 0.001, 0.03, True)
allImages.extend(images)
""
print(f"mating decease: X={X} f(X)={fun}")

for i in range(len(allImages)):
    imageio.imsave(f"./gifs/GA_Test/{i}.jpg", allImages[i])
