import L5.PYGA.Population as POP
import L5.PYGA.NaturalSelection as NS
import numpy as np
import matplotlib.pyplot as pp
from Global.helperFuncs import MakeImage


def indToX(individual, bitlength, bounds):
    """
    maps individual to number in given range
    :param individual:
    :param bitlength: word length of individual
    :param bounds:
    :return: mapped value of individual
    """
    max = 2 ** bitlength - 1
    a, b = bounds
    res = a + individual * (b - a) / max
    assert a <= res <= b
    return res


def GA(fun, bounds, bitLen, populationSize, generationCount, mutationStrength, crossDegree, minimize=False):
    """
    Genertic Algorithm based on code provided by professor
    :param fun: function to optimize
    :param bounds: optimization bound
    :param bitLen: word length of individual
    :param populationSize:
    :param generationCount:
    :param mutationStrength:
    :param crossDegree:
    :param minimize: function needs to be minimized
    :return: optimal point X and function value at X
    """
    newfun = lambda x: fun(indToX(x, bitLen, bounds))
    population = POP.generatePopulation(bitLen, populationSize)
    for i in range(generationCount):
        population = singleIteration(bitLen, crossDegree, minimize, mutationStrength, newfun, population)
    ans, sumval = POP.EvaluateIndividuals(newfun, population, minimize)
    x = np.argmax(ans)
    X = indToX(population[x], bitLen, bounds)
    return X, fun(X)


def singleIteration(bitLen, crossDegree, minimize, mutationStrength, newfun, population):
    newPop = NS.selectGeneration(newfun, population, minimize)
    newPop1 = NS.crossOver(newPop, bitLen, crossDegree)
    population = NS.mutate(newPop1, bitLen, mutationStrength)
    return population


def GA_Printable(figName, fignum, fun, bounds, bitLen, populationSize, generationCount, mutationStrength, crossDegree,
                 minimize=False):
    """
    Genertic Algorithm based on code provided by professor with visualisations
    :param fun: function to optimize
    :param bounds: optimization bound
    :param bitLen: word length of individual
    :param populationSize:
    :param generationCount:
    :param mutationStrength:
    :param crossDegree:
    :param minimize: function needs to be minimized
    :return: optimal point X and function value at X
    """
    images=[]
    boundFormat = 'k--'
    pointFormat = 'bx'
    fig = pp.figure(fignum)
    pp.title(f"{figName}. population change")
    lb, ub = bounds
    pp.ylim((lb - .5, ub + .5))
    pp.xlim((-.5, generationCount + .5))
    pp.xticks(np.arange(0, generationCount + .5, 1))
    pp.plot([-.5, generationCount + .5], [lb, lb], boundFormat)
    pp.plot([-.5, generationCount + .5], [ub, ub], boundFormat)
    newfun = lambda x: fun(indToX(x, bitLen, bounds))
    population = POP.generatePopulation(bitLen, populationSize)
    for j in population:
        pp.plot(0, indToX(j, bitLen, bounds), pointFormat)
    ans, sumval = POP.EvaluateIndividuals(newfun, population, minimize)
    fArg = []
    x = np.argmax(ans)
    X = indToX(population[x], bitLen, bounds)
    fArg.append(fun(X))
    for i in range(generationCount):
        population = singleIteration(bitLen, crossDegree, minimize, mutationStrength, newfun, population)
        ans, sumval = POP.EvaluateIndividuals(newfun, population, minimize)
        for j in population:
            pp.plot(i + 1, indToX(j, bitLen, bounds), pointFormat)
        x = np.argmax(ans)
        X = indToX(population[x], bitLen, bounds)
        fArg.append(fun(X))
    images.append(MakeImage(fig))
    pp.close(fig)
    fig = pp.figure(fignum)
    pp.title(f"{figName}. function change")
    pp.xlim((-.5, generationCount + .5))
    pp.xticks(np.arange(0, generationCount + .5, 1))
    pp.plot(range(generationCount + 1), fArg, 'bx-')
    images.append(MakeImage(fig))
    pp.close(fig)
    return X, fun(X),images
