import math
import numpy as np

def example(x):
    return math.pi * x * x * (.666667 * x + 2)


def function(x):
    return (1 / ((x - 0.16) ** 2 + 0.1)) * np.sin(1 / x)
