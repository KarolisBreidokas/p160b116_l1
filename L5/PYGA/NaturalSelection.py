# THE HAND OF GOD
from L5.PYGA.Population import EvaluateIndividuals
import random as rng
import math


def selectIndividual(wheel: zip, fitSum):
    """
    selects individual using roulette wheel method
    :param wheel: list of (individual,sumval) touple where sumval sum of fitness values from fist to this individual
    :param fitSum: sum of all fitness funcitons
    :return: selected individual
    """
    ball = rng.random() * fitSum
    for individual, fitval in wheel:
        if fitval >= ball:
            return individual
    return individual


def selectGeneration(func, population, minimize: bool):
    """
    creates new population by selecting new individuals based on roulette method
    :param func: fitness function that accepts individual
    :param population:
    :param minimize: fitness function needs to be minimized
    :return: new selected population
    """
    fitness, fitSum = EvaluateIndividuals(func, population, minimize)
    popLength = len(population)
    roulette = [fitness[0]]
    for fitValue in fitness[1:]:
        roulette.append(roulette[-1] + fitValue)
    newPop = []
    for i in range(popLength):
        newPop.append(selectIndividual(zip(population, roulette), fitSum))
    return newPop


def crossOver(population, bitLength, degree):
    """
    implements crossover on selected population
    :param population:
    :param bitLength: word length of individual
    :param degree: crossover degree from 0 to 1
    :return: population with crossover
    """
    popLength = len(population)
    u = math.floor(popLength * degree)
    matingIndecies = []
    for i in range(u // 2):
        matingIndecies.append((rng.randint(0, popLength - 1), rng.randint(0, popLength - 1)))
    for i1, i2 in matingIndecies:
        # let bitlen is 5
        # let splitPos be 3
        # then splitMask is 0b00111
        # ind1                  is [0..0 a1 a2 a3 a4 a5]
        # ind2                  is [0..0 b1 b2 b3 b4 b5]
        # splitMask             is [0..0  0  0  1  1  1]
        # ~splitMask            is [1..1  1  1  0  0  0]
        # (T1) ind1&splitMask   is [0..0  0  0 a3 a4 a5]
        # (T2) ind2&~splitMask  is [0..0 b1 b2  0  0  0]
        # (T1)|(T2)             is [0..0 b1 b2 a3 a4 a5]

        splitPos = rng.randint(1, bitLength - 1)
        splitMask = 2 ** splitPos - 1
        tmp = (population[i1] & splitMask) | (population[i2] & ~splitMask)
        population[i1] = (population[i2] & splitMask) | (population[i1] & ~splitMask)
        population[i2] = tmp
    return population


def mutateIndividual(individual, bitLength, strength):
    """
    mutates selected individual
    :param individual:
    :param bitLength: word length of individual
    :param strength: the mutation strength
    :return: mutated individual
    """
    mask = 1
    for i in range(bitLength):
        if rng.random() <= strength:
            individual = individual ^ mask
        mask = mask << 1  # equivalent to mask=mask*2
    return individual


def mutate(population, bitlength, strength):
    """
    mutate whole population
    :param population:
    :param bitLength: word length of individual
    :param strength: the mutation strength
    :return: mutated population
    """
    MutatedPop = list(map(lambda x: mutateIndividual(x, bitlength, strength), population))
    return MutatedPop
