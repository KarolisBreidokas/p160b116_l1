import random, math
import numpy as np


def generatePopulation(bitLength: int, populationSize: int):
    """
    generates new population
    :param bitLength: word length of individual
    :param populationSize:
    :return: new population
    """
    maxchrom = 2 ** bitLength - 1
    if populationSize >= maxchrom:
        populationSize = maxchrom
    population = []
    for i in range(populationSize):
        population.append(random.randint(0, maxchrom))
    return population


def _sigmoid(x, target, variance):
    return (math.e ** (-(x - target) ** 2 / (2 * variance))) / np.sqrt(2 * math.pi * variance)


def EvaluateIndividuals(func: callable, population: list, minimize: bool):
    """
    evaluated all individuals based on fitness function
    returned fitness value is passed through _sigmoid function
    :param func: fitness function that accepts an individual
    :param population:
    :param minimize: fitness function needs to be minimized
    :return: modified fitness values and sum of all fitness values
    """
    popsize = len(population)
    minimum = math.inf
    maximum = -math.inf
    funcNum = []
    funcSum = 0
    for individual in population:
        tmp = func(individual)
        if tmp > maximum:
            maximum = tmp
        if tmp < minimum:
            minimum = tmp
        funcNum.append(tmp)
        funcSum += tmp
    avg = funcSum / popsize
    sSum = 0
    for i in funcNum:
        sSum += (avg - i) ** 2
    variance = sSum / (popsize - 1) + .2
    target = maximum
    if minimize:
        target = minimum
    ans = _sigmoid(np.array(funcNum), target, variance)
    sumval = sum(ans)
    return ans, sumval
