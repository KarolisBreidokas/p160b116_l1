from deap import base, creator, tools, algorithms
import random, math
from L5.Utils.Point2d import getLenByPermutation, generatePointList
from L5.Utils.PlotConstraints import point_count, yrange, xrange
from L5.Utils.Ploting import PlotMovementByPermutationImage
import imageio

creator.create("FitMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitMin)

gencnt = 100
popsize = 1000
mutationChance=0.06
matingChance=.5

def createIndividual(n):
    ind = creator.Individual()
    for i in range(n):
        ind.append(random.randint(0, n - i - 1))
    return ind


tb = base.Toolbox()
tb.register("individual", createIndividual, point_count)
tb.register("population",
            tools.initRepeat,
            list,
            tb.individual,
            n=popsize)
pl = generatePointList(point_count, xrange, yrange)


def eval(individual):
    return (getLenByPermutation(individual, pl),)


def mutate(individual: list, chance):
    n = len(individual)
    for i in range(n):
        if random.random() < chance:
            individual[i] += (random.randint(0, 1) * 2) - 1
            if individual[i] < 0:
                individual[i] = 0
            if individual[i] > n - i - 1:
                individual[i] = n - i - 1
    return individual,


tb.register("mate", tools.cxTwoPoint)
tb.register("mutate",
            mutate, chance=mutationChance)
tb.register("select",
            tools.selTournament,
            tournsize=3)
tb.register("evaluate", eval)
pop = tb.population()
images = []
for j in range(gencnt):
    npop, tmp = algorithms.eaSimple(pop, tb, matingChance, mutationChance, 1)
    ind = npop[0]
    fun, = eval(ind)
    for i in npop:
        tfun, = eval(i)
        if tfun < fun:
            fun = tfun
            ind = i
    pop = npop
    images.append(PlotMovementByPermutationImage(j, ind, pl))

kwargs_write = {'fps': 1.0, 'quantizer': 'nq'}
imageio.mimsave('./gifs/TSP_Test.gif', images, fps=2)
for i in range(len(images)):
    imageio.imsave(f"./gifs/TSP_Test/{i}.jpg", images[i])
