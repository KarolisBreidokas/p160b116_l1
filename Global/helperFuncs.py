import numpy as np
from matplotlib import pyplot as pp


def make_quiver(point, gradient, color):
    pp.quiver(point[0], point[1], gradient[0], gradient[1], color=color, angles='xy')


def MakeImage(fig):
    fig.canvas.draw()
    image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
    return image.reshape(fig.canvas.get_width_height()[::-1] + (3,))