import L1.Utils.Plot as pl
import L1.Utils.Task2Functions as T2


pl.draw_2d_function_no_df(1, [0.01, 1], [-10, 10], [-10, 10], T2.f1)
pl.draw_2d_function_no_df(2, [0.01, 1], [-10, 10], [-10, 10], T2.f2)
pl.draw_2d_function_no_df(3, [0.01, 1], [-10, 10], [-10, 10], T2.f3)
pl.draw_2d_function_no_df(4, [0.01, 0.1], [-2, 2], [-2, 2], T2.f4)

exit(0)
