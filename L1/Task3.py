import matplotlib.pyplot as pp
import numpy as np
import scipy.optimize as sp
from matplotlib import cm
import L1.Utils.Task2Functions as T2

# Plot.draw_2d_function(1, [1, 100], [-1000, 1000], [-1000, 1000], funcs.f2, funcs.df2)

pnts = [[], []]


def callback(xi):
    global pnts
    pnts[0].append(xi[0])
    pnts[1].append(xi[1])


def draw_2d_with_optimization(fig_num: int, spacing, x: np.array, y: np.array, f: callable, point_init: np.array):
    global pnts
    X = np.arange(x[0], x[1], spacing)
    Y = np.arange(y[0], y[1], spacing)
    X, Y = np.meshgrid(X, Y)
    Z = f(np.array([X, Y]))

    fig = pp.figure(fig_num)
    ax = fig.add_subplot(111)

    pnts = [[point_init[0]], [point_init[1]]]
    ax.axes.set_xlabel("X")
    ax.axes.set_ylabel("Y")
    ax.contour(X, Y, Z, 20, cmap=cm.coolwarm)

    ans = sp.minimize(f, point_init, callback=callback)
    z = f(np.array(pnts)) + 100
    surf = ax.plot(pnts[0], pnts[1], color='red')
    ax.plot([ans.x[0]], [ans.x[1]], marker='o')
    pp.show()
    print(ans)
    print('----')


draw_2d_with_optimization(1, 0.01, [-10, 10], [-10, 10], T2.f1,np.array([3,3]))
draw_2d_with_optimization(2, 0.01, [-10, 10], [-10, 40], T2.f2,np.array([5,5]))
draw_2d_with_optimization(3, 0.01, [-10, 20], [-10, 10], T2.f3,np.array([5,1]))
draw_2d_with_optimization(4, 0.01, [-2, 2], [-2, 2], T2.f4,np.array([-1,-1]))
draw_2d_with_optimization(4, 0.01, [-2, 2], [-2, 2], T2.f4,np.array([1,1]))
exit(0)
