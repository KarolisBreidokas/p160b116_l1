import L1.Utils.AssignedFuncts as fs
import numpy as np
import scipy.optimize as op
import matplotlib.pyplot as pp

ranges = [
    ([-10, 0], 'red'),
    ([0, 10], 'yellow'),
    ([10, 20], 'purple'),
    ([20, 30], 'blue'),
    ([-10, 10], 'blue')
]
pnts = []

xp = np.arange(-10, 30, .1)
points = fs.f1(xp)
df= fs.df1(xp)
pp.figure(1)
pp.plot(xp, points)
pp.plot(xp, df)
pp.plot([-10,30],[0,0])
for rng in ranges:
    pnts = []
    ans = op.minimize_scalar(fs.f1, bounds=rng[0], method='bounded',tol=1e-9, options={'disp': True})
    if ans.success:
        print(f"{rng[0]}:ans={ans.x}")
        pp.plot(ans.x, fs.f1(ans.x), marker='o', color=rng[1])

pp.show()
