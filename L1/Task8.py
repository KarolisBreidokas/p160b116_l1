import L1.Utils.AssignedFuncts as funcs
import matplotlib.pyplot as pp
import numpy as np
from matplotlib import cm
from L1.Utils import AGD

# Plot.draw_2d_function(1, [1, 100], [-1000, 1000], [-1000, 1000], funcs.f2, funcs.df2)

X = np.arange(0.9, 4, .01)
Y = np.arange(-1, 1.1, .01)
X, Y = np.meshgrid(X, Y)
Z = funcs.f2(np.array([X, Y]))

fig = pp.figure()
ax = fig.add_subplot(111)

ax.axes.set_xlabel("X")
ax.axes.set_ylabel("Y")
ax.contour(X, Y, Z, 200, cmap=cm.coolwarm)

ans = AGD.conjugate_grad_descent(funcs.f2, funcs.df2, np.array([1, 1]), 60)
z = funcs.f2(np.array(ans.prev)) + 100
surf = ax.plot(ans.prev[0], ans.prev[1], color='red')
ax.plot([ans.x[0]], [ans.x[1]], marker='o')
pp.show()

print([ans.x[0], ans.x[1], ans.fun, len(ans.prev[0])])
# exit(0)
