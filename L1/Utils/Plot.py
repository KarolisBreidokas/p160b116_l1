import matplotlib.pyplot as pp
import numpy as np


def draw_2d_function(fig_num: int, spacing, x: np.array, y: np.array, f: callable, df: callable):
    xp = np.arange(x[0], x[1], spacing[0])
    yp = np.arange(y[0], y[1], spacing[0])
    xx, xy = np.meshgrid(xp, yp)
    p = np.array([xx, xy])
    dx = np.arange(x[0], x[1], spacing[1])
    dy = np.arange(y[0], y[1], spacing[1])
    dxx, dxy = np.meshgrid(dx, dy)
    dp = np.array([dxx, dxy])
    zx = df(dp)
    z = f(p)
    draw_2d(fig_num, p, z, dp, zx)


def draw_2d_function_no_df(fig_num: int, spacing, x: np.array, y: np.array, f: callable):
    xp = np.arange(x[0], x[1], spacing[0])
    yp = np.arange(y[0], y[1], spacing[0])
    xx, xy = np.meshgrid(xp, yp)
    p = np.array([xx, xy])
    dx = np.arange(x[0], x[1], spacing[1])
    dy = np.arange(y[0], y[1], spacing[1])
    dxx, dxy = np.meshgrid(dx, dy)
    dp = np.array([dxx, dxy])
    dz = f(dp)
    z = f(p)
    zx = np.gradient(dz)
    draw_2d(fig_num, p, z, dp, np.array([zx[1], zx[0]]))


def draw_2d(fig_num: int, p: np.array, f: np.array, dp: np.array, df: np.array):
    pp.figure(fig_num)
    pp.contour(p[0], p[1], f,10)
    pp.quiver(dp[0], dp[1], df[0], df[1])
    pp.show()

