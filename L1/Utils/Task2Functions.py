import numpy as np


def f1(x: np.array):
    return x[0] ** 2 + 100 * x[1] ** 2


def f2(x: np.array):
    return 100 * (x[1] - x[0] ** 2) ** 2 + (1 - x[0]) ** 2


def f3(x: np.array):
    return np.sin(x[0] ** 2 + 3 * x[1] ** 2 + 1) / (x[0] ** 2 + 3 * x[1] ** 2 + 1)


def df3(x: np.array):
    return np.array([
        (2 * x[0]) * (np.cos(x[0] ** 2 + 3 * x[1] ** 2 + 1) -
                      np.sin(x[0] ** 2 + 3 * x[1] ** 2 + 1)) /
        ((x[0] ** 2 + 3 * x[1] ** 2 + 1) ** 2)
        ,
        (6 * x[1]) * (np.cos(x[0] ** 2 + 3 * x[1] ** 2 + 1) -
                      np.sin(x[0] ** 2 + 3 * x[1] ** 2 + 1)) /
        ((x[0] ** 2 + 3 * x[1] ** 2 + 1) ** 2)

    ])


def f4(x: np.array):
    return x[0] * np.exp(-(x[0] ** 2) - (x[1] ** 2))
