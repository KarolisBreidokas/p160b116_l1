import numpy as np
import numpy.linalg as alg
import scipy.optimize as op
import math


class answer:
    def __init__(self, ix, fun):
        self.x = ix
        self.prev = [[ix[0]], [ix[1]]]
        self.fun = fun

    def insert(self, x, fun):
        self.x = x
        self.prev[0].append(x[0])
        self.prev[1].append(x[1])
        self.fun = fun


def anti_gradient_descent(f: callable, df: callable, x: np.array, gamma: float, stepc: int):
    np.seterr(all='raise')
    ans = answer(x, f(x))
    while stepc > 0:
        stepc -= 1
        gd = df(x)
        x = x - gamma * gd
        ans.insert(x, f(x))
    return ans


def anti_gradient_descent_cs(f: callable, df: callable, x: np.array, gamma: float, stepc: int):
    ans = answer(x, f(x))
    while stepc > 0:
        stepc -= 1
        gd = df(x)
        S = -gd / (np.sqrt(np.sum(gd ** 2)))
        x = x + gamma * S
        ans.insert(x, f(x))
    return ans


def steepest_agd(f: callable, df: callable, x: np.array):
    ans = answer(x, f(x))
    tf = math.inf
    gd = df(x)
    while tf > f(x) and (np.sqrt(np.sum(gd ** 2))) > 1e-9:
        tf = f(x)
        S = -gd / (np.sqrt(np.sum(gd ** 2)))
        gamma = op.minimize_scalar(lambda g: f(x + g * S)).x
        x = x + gamma * S
        gd = df(x)
        ans.insert(x, tf)
    return ans


def conjugate_grad_descent(f: callable, df: callable , x: np.array, stepc):
    ans = answer(x, f(x))
    G = -df(x)
    S = G
    while stepc > 0:
        stepc -= 1
        gamma = op.minimize_scalar(lambda g: f(x + g * S)).x
        x = x + gamma * S
        nG = -df(x)
        b = (nG @ nG) / (G @ G)
        S = nG + b * S
        ans.insert(x, f(x))
    return ans


def newton_optimization(f: callable, df: callable, hf: callable, x: np.array):
    ans = answer(x, f(x))

    tf = math.inf
    gd = df(x)
    while (np.sqrt(np.sum(gd ** 2))) > 1e-9:
        g = df(x)
        h = hf(x)
        x = x - g @ alg.inv(h)

        tf = f(x)
        gd = df(x)
        ans.insert(x, f(x))
    return ans
