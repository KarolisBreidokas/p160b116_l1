import numpy as np
import scipy.optimize as op
import scipy.linalg as alg

A = np.array([
    [3, 1, 0], [1, 2, -1], [0, -1, 5],
])
B = np.array([1, 2, 3])


def f(x):
    global A, B
    return 1 / 2 * x @ A @ x - B @ x


def df(x):
    global A, B
    return A @ x - B
C= alg.det( A)
cc=alg.inv(A)*C
x = np.array([-2, 1, 3])

ccc=df(x)
afs=cc@ccc
anst=x*C-ccc@cc

tf = f(x)
S =-df(x)

gamma = op.minimize_scalar(lambda g: f(x + g * S)).x
x = x + gamma * S
gdt = f(x)
print(gamma)
