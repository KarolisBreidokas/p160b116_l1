from L4.Utils.AssignedFuncts import f, constraints
from L4.Utils.helperFuncs import BasicPlot
from Global.helperFuncs import MakeImage
from L4.Utils.CPM_2d import CPM_2d,findX

import L4.Utils.PlotConsts as cns
import numpy as np
import matplotlib.pyplot as pp
import imageio
import imageio_ffmpeg as iio

images = []
points = [[0, -3], [0, 3], [5, 3], [5, -3]]
plot_points = points.copy()
plot_points.append(points[0])
plot = [[i[j] for i in plot_points] for j in range(2)]
fig = BasicPlot(-1)
pp.plot(plot[0], plot[1], cns.style_feasable)

for i in range(5):
    X1, points = CPM_2d(f, constraints, np.array(points))
    pp.plot(X1[0], X1[1], cns.style_point)
    # pp.show()
    plot_points = points.copy()
    plot_points.append(points[0])
    plot = [[i[j] for i in plot_points] for j in range(2)]
    image = MakeImage(fig)
    images.append(image)
    fig = BasicPlot(i)
    pp.plot(plot[0], plot[1], cns.style_feasable)

X1=findX(f,points)
pp.plot(X1[0], X1[1], cns.style_point)
image = MakeImage(fig)
images.append(image)
# pp.show()

kwargs_write = {'fps': 1.0, 'quantizer': 'nq'}
imageio.mimsave('./gifs/CPM_Test_2d.gif', images, fps=1)
new_images=images.copy()
new_images.append(images[-1])
imageio.mimwrite('./gifs/CPM_Test_2d.mp4', new_images, fps=1)

for i in range(len(images)):
    imageio.imsave(f"./gifs/CPM_Test_2d/{i}.jpg", images[i])