import numpy as np


class Constraint:
    def __init__(self, f: callable, df: callable):
        self.f = f
        self.df = df


def f(x: np.array):
    return -x[0] - 3 * x[1]


def g1(x: np.array):
    return 2 * x[0] - (x[1]) ** 2 - 1.1


def dg1(x: np.array):
    return np.array(
        [
            2,
            -2 * x[1],
        ]
    )


def g2(x: np.array):
    return 9 - 0.8 * ((x[0]) ** 2) - 2 * x[1]


def dg2(x: np.array):
    return np.array(
        [
            -0.8 * 2 * x[0],
            -2,
        ]
    )


constraints = [
    Constraint(g1, dg1),
    Constraint(g2, dg2),
]
