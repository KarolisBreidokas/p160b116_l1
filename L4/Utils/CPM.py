from scipy.optimize import linprog
from typing import List
from L4.Utils.AssignedFuncts import Constraint
import scipy.optimize as sp
import numpy as np


class LinConstraint:
    # sum(ai*xi)+c>=0
    def __init__(self, a_xoef, c):
        self.a = np.array(a_xoef)
        self.c = c

    def calc(self, x):
        res = 0
        for a, x in zip(self.a, x):
            res = res + a * x
        return res + self.c

    def disp(self, xl, xh, yl, yh):
        points = []
        if not np.allclose(self.a[1], 0):
            y = (-self.c - self.a[0] * xl) / self.a[1]
            if yl <= y <= yh:
                points.append([xl, y])
            y = (-self.c - self.a[0] * xh) / self.a[1]
            if yl <= y <= yh:
                points.append([xh, y])
        if not np.allclose(self.a[0], 0):
            x = (-self.c - self.a[1] * yl) / self.a[0]
            if xl <= x <= xh:
                points.append([x, yl])
            x = (-self.c - self.a[1] * yh) / self.a[0]
            if xl <= x <= xh:
                points.append([x, yh])
        return points


f1 = LinConstraint(np.array([-1, -3]), 0)


def findX(f: LinConstraint, lcons: List[LinConstraint]):
    c = f.a
    A = []
    B = []
    dims = 0
    for cons in lcons:
        dims = max(dims, len(cons.a))
        A.append(cons.a)
        B.append(-cons.c)
    # noinspection PyTypeChecker
    ans = sp.linprog(c, A_ub=A, b_ub=B, bounds=(None, None))
    if not ans.success:
        return None
    return ans.x


def CPM(f: LinConstraint, constraints: List[Constraint], lcons: List[LinConstraint]):
    c = f.a
    A = []
    B = []
    dims = 0
    for cons in lcons:
        dims = max(dims, len(cons.a))
        A.append(cons.a)
        B.append(-cons.c)
    # noinspection PyTypeChecker
    ans = sp.linprog(c, A_ub=A, b_ub=B, bounds=(None, None))
    if not ans.success:
        return None
    X1 = ans.x
    mostViolated = constraints[0]
    for constraint in constraints[1:]:
        if mostViolated.f(X1) > constraint.f(X1):
            mostViolated = constraint
    g = mostViolated.f(X1)
    dg = mostViolated.df(X1)
    g1 = LinConstraint(-dg, -g + sum(dg * X1))
    A.append(g1.a)
    B.append(-g1.c)
    nlcs = []
    for lc in lcons:
        # noinspection PyTypeChecker
        ans = sp.linprog(-lc.a, A_ub=A, b_ub=B, bounds=(None, None))
        if not ans.success:
            return None
        if np.abs(ans.fun - lc.c) <= 1e-10:
            nlcs.append(lc)
    nlcs.append(g1)
    return X1, nlcs
