import numpy as np
from typing import List
from L4.Utils.AssignedFuncts import Constraint


def getCoeficients(x1: np.array, x2: np.array):
    d = x1 - x2
    d_norm = d / np.sqrt(sum(d * d))
    return d_norm[1], -d_norm[0], -d_norm[1] * x1[0] + d_norm[0] * x1[1]


def findIntersect(l1, l2):
    a1, b1, c1 = l1
    a2, b2, c2 = l2
    a = np.array([
        [a1, b1],
        [a2, b2],
    ])
    b = np.array(
        [
            -c1, -c2
        ]
    )
    try:
        return np.linalg.solve(a, b)
    except:
        return None


def between(l, c, r):
    for _l, _c, _r in zip(l, c, r):
        if _l > _r:
            if _c > _l or _r > _c:
                return False
        else:
            if _c > _r or _l > _c:
                return False
    return True


def calc(l, x):
    a, b, c = l
    return a * x[0] + b * x[1] + c


def findX(targetFun: callable, points: np.array):
    X1 = points[0]
    for point in points[1:]:
        if targetFun(X1) > targetFun(point):
            X1 = point
    return X1


def CPM_2d(targerFun: callable, constraints: List[Constraint], points: np.array):
    X1 = findX(targerFun, points)
    mostViolated = constraints[0]
    for constraint in constraints[1:]:
        if mostViolated.f(X1) > constraint.f(X1):
            mostViolated = constraint
    g = mostViolated.f(X1)
    dg = mostViolated.df(X1)
    g1 = dg[0], dg[1], g - sum(dg * X1)
    found = False
    newPoints = []
    for i, point in enumerate(points[:-1]):
        cl_cofs = getCoeficients(points[i], points[i + 1])
        intercectPoint = findIntersect(g1, cl_cofs)
        if calc(g1, point) > 0:
            newPoints.append(point)
        if intercectPoint is not None:
            if between(points[i], intercectPoint, points[i + 1]):
                newPoints.append(intercectPoint)
    cl_cofs = getCoeficients(points[-1], points[0])
    intercectPoint = findIntersect(g1, cl_cofs)
    if calc(g1, points[-1]) > 0:
        newPoints.append(points[-1])
    if intercectPoint is not None:
        if between(points[-1], intercectPoint, points[0]):
            newPoints.append(intercectPoint)
    return X1, newPoints
