from matplotlib import pyplot as pp

import L4.Utils.AssignedFuncts as AF
import numpy as np

from L4.Utils import PlotConsts as cns


def make_quiver(point, gradient, color):
    pp.quiver(point[0], point[1], gradient[0], gradient[1], color=color, angles='xy')


def BasicPlot(i=1):
    xp = np.arange(cns.xl, cns.xh, cns.step)
    yp = np.arange(cns.yl, cns.yh, cns.step)
    xx, xy = np.meshgrid(xp, yp)
    X = np.array([xx, xy])
    func_points = AF.f(X)
    g1_points = AF.g1(X)
    g2_points = AF.g2(X)
    pp.xlim(cns.xl, cns.xh)
    pp.ylim(cns.yl, cns.yh)
    figure = pp.figure(i)
    pp.contour(xp, yp, func_points, cns.ISOLines)
    pp.contour(xp, yp, g1_points, [0, 0.1], colors=['red', 'blue'])
    pp.contour(xp, yp, g2_points, [0, 0.1], colors=['green', 'blue'])
    return figure


def linCons_plot(lincs):
    for i in lincs:
        points = i.disp(cns.xl, cns.xh, cns.yl, cns.yh)
        pp.plot([points[0][0],points[1][0],],[points[0][1],points[1][1],],cns.style_feasable)