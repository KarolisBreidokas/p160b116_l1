from L4.Utils.AssignedFuncts import constraints
from L4.Utils.helperFuncs import BasicPlot, linCons_plot
from Global.helperFuncs import MakeImage
from L4.Utils.CPM import CPM, f1, LinConstraint,findX

import L4.Utils.PlotConsts as cns
import numpy as np
import matplotlib.pyplot as pp
import imageio

images = []

Linconstraints = [
    LinConstraint([0, 1], -3),
    LinConstraint([0, -1], -3),
    LinConstraint([-1, 0], 0),
    LinConstraint([1, 0], -5),
]
fig = BasicPlot(-1)
linCons_plot(Linconstraints)

for i in range(5):
    X1, Linconstraints = CPM(f1, constraints, Linconstraints)
    print(len(Linconstraints))
    pp.plot(X1[0], X1[1], cns.style_point)

    image = MakeImage(fig)
    images.append(image)
    # pp.show()
    fig = BasicPlot(i)
    linCons_plot(Linconstraints)

X1=findX(f1,Linconstraints)
pp.plot(X1[0], X1[1], cns.style_point)
fig.canvas.draw()
image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
images.append(image)
# pp.show()

kwargs_write = {'fps': 1.0, 'quantizer': 'nq'}
imageio.mimsave('./gifs/CPM_Test.gif', images, fps=1)

new_images = images.copy()
new_images.append(images[-1])
imageio.mimwrite('./gifs/CPM_Test.mp4', new_images, fps=1)
for i in range(len(images)):
    imageio.imsave(f"./gifs/CPM_Test/{i}.jpg", images[i])
