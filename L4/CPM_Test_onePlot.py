from L4.Utils.AssignedFuncts import f, constraints
from L4.Utils.helperFuncs import BasicPlot,linCons_plot
from L4.Utils.CPM import CPM, f1, LinConstraint,findX

import L4.Utils.PlotConsts as cns
import numpy as np
import matplotlib.pyplot as pp


Linconstraints = [
    LinConstraint([0, 1], -3),
    LinConstraint([0, -1], -3),
    LinConstraint([-1, 0], 0),
    LinConstraint([1, 0], -5),
]
BasicPlot()
X=[[],[]]
for i in range(5):
    X1, Linconstraints = CPM(f1, constraints, Linconstraints)
    print(len(Linconstraints))
    X[0].append(X1[0])
    X[1].append(X1[1])
X1=findX(f1,Linconstraints)
X[0].append(X1[0])
X[1].append(X1[1])
pp.plot(X[0], X[1], cns.style_point)
linCons_plot(Linconstraints)
pp.show()

