from L4.Utils.AssignedFuncts import f, constraints
from L4.Utils.helperFuncs import BasicPlot
from L4.Utils.CPM_2d import CPM_2d,findX

import L4.Utils.PlotConsts as cns
import numpy as np
import matplotlib.pyplot as pp

points = [[0, -3], [0, 3], [5, 3], [5, -3]]
X=[[],[]]
for i in range(5):
    X1, points = CPM_2d(f, constraints, np.array(points))
    X[0].append(X1[0])
    X[1].append(X1[1])
X1=findX(f,np.array(points))
X[0].append(X1[0])
X[1].append(X1[1])
plot_points = points.copy()
plot_points.append(points[0])
plot = [[i[j] for i in plot_points] for j in range(2)]
BasicPlot()
pp.plot(plot[0], plot[1], cns.style_feasable)
pp.plot(X[0],X[1],cns.style_path)
pp.show()
