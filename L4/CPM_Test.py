from L4.Utils.AssignedFuncts import f, constraints
from L4.Utils.helperFuncs import BasicPlot,linCons_plot
from L4.Utils.CPM import CPM, f1, LinConstraint,findX

import L4.Utils.PlotConsts as cns
import matplotlib.pyplot as pp


Linconstraints = [
    LinConstraint([0, 1], -3),
    LinConstraint([0, -1], -3),
    LinConstraint([-1, 0], 0),
    LinConstraint([1, 0], -5),
]
BasicPlot()
linCons_plot(Linconstraints)
for i in range(5):
    X1, Linconstraints = CPM(f1, constraints, Linconstraints)
    print(len(Linconstraints))
    pp.plot(X1[0], X1[1], cns.style_point)
    pp.show()
    BasicPlot()
    linCons_plot(Linconstraints)

X1=findX(f1,Linconstraints)
pp.plot(X1[0], X1[1], cns.style_point)
pp.show()


