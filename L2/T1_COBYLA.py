import L2.Utils.AssignedFuncts as AF
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as pp

from scipy.optimize import NonlinearConstraint as NC

from L2.Utils.HelperFuncs import BasePlot, X0
from Global.helperFuncs import make_quiver

BasePlot()
# array for function movement traceback
movement = [[X0[0]], [X0[1]]]

# noinspection PyTypeChecker
t = opt.minimize(AF.t1f,
                 np.array([X0[0], X0[1]]),
                 constraints=NC(lambda x: [AF.t1cf1(x), AF.t1cf2(x)],
                                [-np.inf, -np.inf], [0, 0]),

                 tol=1e-9,
                 method='COBYLA', options={'disp': True}
                 )
movement[0].append(t.x[0])
movement[1].append(t.x[1])
pp.plot(movement[0], movement[1], 'go-')
print(t)

fq = AF.t1df(t.x)

if np.allclose(AF.t1cf1(t.x), 0):
    dq = AF.t1cdf1(t.x)
    make_quiver(t.x, dq, 'red')

if np.allclose(AF.t1cf2(t.x), 0):
    dq = AF.t1cdf2(t.x)
    make_quiver(t.x, dq, 'red')

make_quiver(t.x, fq, 'black')
print("(df X dg) ",fq[0] * dq[1] - fq[1] * dq[0])
for x in zip(movement[0], movement[1]):
    print(x)
pp.show()
