import numpy as np
from matplotlib import pyplot as pp

from L2.Utils import AssignedFuncts as AF

X0 = [.35, .75]


def BasePlot():
    # print cnf
    # tmp = lambda x: np.maximum(np.sign(AF.t1cf1(x)), np.sign(AF.t1cf2(x)))
    xp = np.arange(-0.5, 1.5, 0.01)
    yp = np.arange(-0.5, 1.5, 0.01)
    xx, xy = np.meshgrid(xp, yp)
    p = np.array([xx, xy])
    z = AF.t1f(p)
    c1z = AF.t1cf1(p)
    c2z = AF.t1cf2(p)
    # t3 = tmp(p)
    pp.figure(1)
    pp.contour(xp, yp, z, 20)
    colors = ['blue', 'red']
    pp.contour(xp, yp, c1z, [-0.1, 0], colors=colors)
    pp.contour(xp, yp, c2z, [-0.1, 0], colors=colors)
    # pp.contour(xp, yp, t3, [0], colors="red")


def Part2_plot():
    pp.figure(1)
    xp = np.arange(-3.5, 1.5, 0.1)
    yp = np.arange(-3.5, 1.5, 0.1)
    xx, xy = np.meshgrid(xp, yp)
    p = np.array([xx, xy])
    z = AF.t2f(p)
    c1z = AF.t2P_disp(p)
    pp.contour(xp, yp, c1z, [0, 0.1], colors=["red", "blue"])
    pp.contour(xp, yp, z, 20)