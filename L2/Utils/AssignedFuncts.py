import numpy as np


def t1f(x: np.array):
    return 0.26 * (x[0] ** 2 + x[1] ** 2) - .48 * x[0] * x[1]


def t1df(x: np.array):
    return np.array([
        0.52 * x[0] - .48 * x[1],
        0.52 * x[1] - .48 * x[0],
    ])


def t1cf1(x: np.array):
    return 1 - x[0] - x[1]


def t1cdf1(x: np.array):
    return np.array([
        -1,
        -1,
    ])


def t1cf2(x: np.array):
    return x[0] ** 2 + x[1] ** 2 - 1.5


def t1cdf2(x: np.array):
    return np.array([
        2 * x[0],
        2 * x[1],
    ])


def t2f(x: np.array):
    return x[0] ** 2 + x[1] ** 2


def t2df(x: np.array):
    return np.array([
        2 * x[0],
        2 * x[1],
    ])


def t2PF1(x: np.array):
    return (x[0] + x[1] + 2) ** 2


def t2P_disp(x: np.array):
    return x[0] + x[1] + 2


def t2dP_disp(x: np.array):
    return np.array([
        1,
        1,
    ])


def t2PDf1(x: np.array):
    return np.array([
        2 * (x[0] + x[1] + 2),
        2 * (x[0] + x[1] + 2),
    ])


def t2PF2(x: np.array):
    return np.maximum(0, (x[0] + x[1] + 2)) ** 2


def t2PDf2(x: np.array) -> np.array:
    return np.array([
        2 * np.maximum(0, (x[0] + x[1] + 2)),
        2 * np.maximum(0, (x[0] + x[1] + 2)),
    ])
