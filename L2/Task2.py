import L2.Utils.AssignedFuncts as AF
import L1.Utils.AGD as AGD
import numpy as np
import matplotlib.pyplot as pp

from L2.Utils.HelperFuncs import Part2_plot


def P(x, p):
    return AF.t2f(x) + AF.t2PF1(x) / p


def Dp(x, p):
    return AF.t2df(x) + AF.t2PDf1(x) / p


Part2_plot()

P_val = .25
Ps = [5]
f = lambda x: P(x, P_val)
df = lambda x: Dp(x, P_val)

px = np.array([-1, -2])
steps = [[px[0]], [px[1]]]
agdans = AGD.steepest_agd(f, df, px)
dx = agdans.x
steps[0].append(dx[0])
steps[1].append(dx[1])
sc = len(agdans.prev)
t = 0
while np.linalg.norm(dx - px) > 1e-9:
    px = dx
    P_val /= 2
    Ps.append(P_val)
    agdans = AGD.steepest_agd(f, df, px)
    sc += len(agdans.prev)
    dx = agdans.x
    steps[0].append(dx[0])
    steps[1].append(dx[1])
    sc -= 1
    t += 1
    print(t, "df(X*)=", AF.t2df(dx), "dg(X*)=", AF.t2dP_disp(dx), "dp=", df(dx),"P=",P_val)
pp.plot(steps[0], steps[1], marker='o')
pp.show()
print("agdAns=", sc, "X*=", dx)
print("P=", 1 / P_val)
print("df(X*)=", AF.t2df(dx))
print("dg(X*)=", AF.t2dP_disp(dx))
