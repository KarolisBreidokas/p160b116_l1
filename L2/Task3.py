import L2.Utils.AssignedFuncts as AF
import L1.Utils.AGD as AGD
import L1.Utils.Plot as plt
import numpy as np
import matplotlib.pyplot as pp

from L2.Utils.HelperFuncs import Part2_plot


def P(x, p):
    return AF.t2f(x) + AF.t2PF2(x) / p


def Dp(x, p):
    return AF.t2df(x) + AF.t2PDf2(x) / p


Part2_plot()

P_val = 1
f = lambda x: P(x, P_val)
df = lambda x: Dp(x, P_val)

px = np.array([-1, -2])
steps = [[px[0]], [px[1]]]
agdans = AGD.steepest_agd(f, df, px)
dx = agdans.x
steps[0].append(dx[0])
steps[1].append(dx[1])
sc = len(agdans.prev)
tmpstps = 10
while np.linalg.norm(dx - px) > 1e-9:
    px = dx
    P_val /= 5
    agdans = AGD.steepest_agd(f, df, px)
    sc += len(agdans.prev)
    dx = agdans.x
    steps[0].append(dx[0])
    steps[1].append(dx[1])
pp.plot(steps[0], steps[1], marker='o')
pp.show()
print("agdAns=", sc, "ans=", dx)
exit(0)
